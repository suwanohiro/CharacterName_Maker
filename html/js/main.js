window.onload = function () {
    //console.log(output);
    display_move.Change_display("main_display");
    button.make();
    addEvent();

    setInterval(function () {
        var flg = conditions.information;
        console.log(`設定情報(苗字) : ${flg[0]}\n設定情報(名前) : ${flg[1]}`);
    }, 1000);
}

function addEvent() {
    //body
    window.addEventListener("keydown", (e) => {
        var displays = document.getElementById("main_display").style.display;

        switch (e.keyCode) {
            case 13:
                if (String(displays) == "block") {
                    document.getElementById("make").click();
                }
                break;

            default:
                break;
        }
    });

    //main_display
    document.getElementById("to_setting").addEventListener("click", () => {
        button.setting_cash();
        display_move.Change_display("setting");
    });

    document.getElementById("to_how_to_use").addEventListener("click", () => {
        display_move.Change_display("how_to_use");
    })

    document.getElementById("make").addEventListener("click", () => {
        button.make();
    });


    //setting
    document.getElementById("setting_saves").addEventListener("click", (e) => {
        if (e.target.id == "setting_saves") {
            display_move.Change_display("main_display");
            button.setting_save();
            console.log("saved!");
        }
    });

    document.getElementById("setting_cancel").addEventListener("click", () => {
        display_move.Change_display("main_display");
        button.setting_load();
    });

    document.getElementById("make_set1").addEventListener("change", () => {
        document.getElementById("make_set2").value = document.getElementById("make_set1").value;
    });

    document.getElementById("make_set2").addEventListener("change", () => {
        document.getElementById("make_set1").value = document.getElementById("make_set2").value;
    });

    //使用方法
    document.getElementById("how_to_use_to_main_display").addEventListener("click", () => {
        display_move.Change_display("main_display");
    });
}

class button {
    static Number_of_generated = 10;
    static cash = new Array(4).fill("");

    static Set_Number_of_generated(num) {
        this.Number_of_generated = Number(num);
    }

    static Get_Number_of_generated() {
        return this.Number_of_generated;
    }

    static make() {
        var num = this.Get_Number_of_generated();
        Names.DataBase = undefined;
        var output = Names.Output(num);
        document.getElementById("main_content").innerHTML = "";
        for (var a = 0; a < output.length; a++) {
            element.make(output[a]);
        }
    }

    static setting_save() {
        var make_element = document.getElementById("make");
        var setting_input = document.getElementsByClassName("setting_input");
        var data = new Array(setting_input.length);
        var array = conditions.information;
        for (var a = 0; a < data.length; a++) {
            data[a] = Number(setting_input[a].value);
        }

        //生成個数
        if (data[0] > 500) data[0] = 500;
        this.Set_Number_of_generated(data[0]);

        //苗字 国
        array[0][1] = data[1];

        //名前 性別
        array[1][0] = data[2];

        //名前 国
        array[1][1] = data[3];

        conditions.information = array;

        name_make.Make_DataBase();

        make_element.click();
    }

    static setting_cash() {
        var setting_input = document.getElementsByClassName("setting_input");
        var data = new Array(4);
        for (var a = 0; a < setting_input.length; a++) {
            var work = setting_input[a].value;
            if (work == "") work = 1;
            if (work == "" && a == 0) work *= 10;
            data[a] = Number(work);
        }
        this.cash = data;
    }

    static setting_load() {
        var setting_input = document.getElementsByClassName("setting_input");
        var data = this.cash;
        for (var a = 0; a < setting_input.length; a++) {
            var work = data[a];
            if (work == "") work = 1;
            if (work == "" && a == 0) work *= 10;
            setting_input[a].value = Number(work);
        }
    }
}

class element {
    static make(data) {
        var output_id = "main_content";
        var output = document.getElementById(output_id);
        var tr_class = document.getElementsByClassName("tr_class");

        output = element.make_div(output);
        output = element.make_table(output);
        output = element.make_tr(output);
        this.make_td(output, tr_class.length, 0)
        for (var a = 0; a < 3; a++) {
            element.make_td(output, data[a], 1);
        }

        output = document.getElementById(output_id);
    }

    static make_div(output) {
        var div_class = document.getElementsByClassName("div_class");
        var div = document.createElement("div");
        var output_id = `result_${div_class.length}`;
        div.id = output_id;
        div.className = "div_class";
        div.style.height = `10vh`;
        div.style.marginTop = `1vh`;
        div.style.marginBottom = `1vh`;
        div.style.paddingTop = "1vh";
        div.style.paddingBottom = "1vh";
        div.style.backgroundColor = "lemonchiffon"
        output.appendChild(div);

        return document.getElementById(output_id);
    }

    static make_table(output) {
        var table_class = document.getElementsByClassName("table_class");
        var table = document.createElement("table");
        var output_id = `table_${table_class.length}`;
        table.id = output_id;
        table.className = "table_class";
        table.style.width = `100%`;
        table.style.height = `100%`;

        output.appendChild(table);

        return document.getElementById(output_id);
    }

    static make_tr(output) {
        var tr_class = document.getElementsByClassName("tr_class");
        var tr = document.createElement("tr");
        var output_id = `tr_${tr_class.length}`;
        tr.id = output_id;
        tr.className = "tr_class";

        output.appendChild(tr);

        return document.getElementById(output_id);
    }

    static make_td(output, str, flg) {
        var tr_class = document.getElementsByClassName("tr_class");
        var td_class = document.getElementsByClassName(`td_${tr_class.length}`);
        var td = document.createElement("td");
        if (flg == 1) {
            td.style.height = "100%";
            td.style.border = "0.0125vw solid";
            var id = `td_${tr_class.length}_${td_class.length}`;
            td.id = id;
            td.className = `td_${tr_class.length} names`;
            td.style.borderRadius = "1vh";
            td.innerHTML = str;
            td.onclick = function () {
                var title_txt = document.getElementById("title_txt");
                copy.start(this.innerHTML);
                title_txt.innerHTML = "コピーしました";
                setTimeout(function () {
                    var title_txt = document.getElementById("title_txt");
                    title_txt.innerHTML = "キャラネームメーカー";
                }, 625);
            }
        } else {
            td.style.fontSize = "2vh";
            td.style.textAlign = "right";
            td.style.paddingRight = "1vw";
        }

        var w = 31;
        if (flg == 0) w = 7;
        td.style.width = `${w}%`;
        td.innerHTML = str;

        output.appendChild(td);
    }
}

class copy {
    static start(text) {
        // テキストコピー用の一時要素を作成
        const pre = document.createElement('pre');

        // テキストを選択可能にしてテキストセット
        pre.style.webkitUserSelect = 'auto';
        pre.style.userSelect = 'auto';
        pre.textContent = text;

        // 要素を追加、選択してクリップボードにコピー
        document.body.appendChild(pre);
        document.getSelection().selectAllChildren(pre);
        const result = document.execCommand('copy');

        // 要素を削除
        document.body.removeChild(pre);

        return result;
    }
}

class Names {
    static Output(num) {
        if (num == undefined) return undefined;
        var output = [];
        for (var a = 0; a < num; a++) {
            var data = name_make.make();
            if (data != undefined) {
                var flg = index.Array(output, data);
                if (flg == -1) {
                    //正常動作した場合
                    output[a] = data;
                } else {
                    //既に選ばれた名前を出した場合
                    a--;
                }
            }
        }
        return output;
    }
}

class index {
    static Array(data1, data2) {
        var result = String(data1).indexOf(String(data2));
        result = (Number(result) / (index.len(data1) * 2));
        if (result < 0 || data1[result].length != data2.length) return -1;
        return Number(result);
    }

    static len(data) {
        var max = 0;
        for (var a = 0; a < data.length; a++) {
            if (max < data[a].length) max = data[a].length;
        }
        return max;
    }
}

class csv {
    static get_csv_data() {
        var csv_files = text.array_read("settings/csv_files.ini");
        var get_data = new Array(csv_files.length);
        for (var a = 0; a < csv_files.length; a++) {
            get_data[a] = text.csv_read(`csv/${csv_files[a]}`);
        }
        return get_data;
    }
}

class text {
    static normal_read(url) {
        var txt = new XMLHttpRequest();
        txt.open('get', url, false);
        txt.send();
        return txt.responseText;
    }

    static array_read(url) {
        var txt = new XMLHttpRequest();
        txt.open('get', url, false);
        txt.send();
        return txt.responseText.split(/\r\n|\n/);
    }

    static csv_read(url) {
        var txt = new XMLHttpRequest();
        txt.open('get', url, false);
        txt.send();
        var arr = txt.responseText.split(/\r\n|\n/);
        var res = [];
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == '') break;
            res[i] = arr[i].split(',');
        }
        return res;
    }
}

class random {
    static old_random = 0;

    static make(min, max) {
        if (max == undefined) {
            max = min;
            min = 0;
        }
        if (max < min) {
            var work = max;
            max = min;
            min = work;
        }
        return Math.floor(Math.random() * (max + 1 - min)) + min;
    }

    static make_pro(min, max) {
        var result = this.old_random;
        while (result == this.old_random) {
            result = this.make(min, max);
        }
        this.old_random = result;
        return result;
    }
}

//条件取得
class conditions {
    //0:両方 | 1:男/日本/true | 2:女/西洋/false
    static information = [
        //性別, 国, 地名, 自然, 色, 動物
        [0, 1, 0, 0, 0, 0], //苗字
        [2, 1, 0, 0, 0, 0] //名前
    ];

    static get(str, num) {
        if (num >= this.information.length || num == undefined) {
            console.error("conditions.get : Error\nnum : undefined\n適切な情報取得の添字を入力してください。");
            return undefined;
        }
        //苗字側の性別設定を常に「両方」に
        this.information[0][0] = 0;

        var input = str.split(",");
        var data = ["性別", "国", "地名", "自然", "色", "動物"];
        var output = new Array(input.length).fill("");
        if (str == "all") input = data;
        for (var a = 0; a < input.length; a++) {
            var b = data.indexOf(input[a]);
            if (b < 0) return undefined;
            output[a] = this.information[num][b];
        }
        if (output.length == 1) output = output[0];
        return output;
    }
}

class name_make {
    static DataBase = undefined;

    static make() {
        if (this.DataBase == undefined) this.Make_DataBase();
        var data = this.DataBase;
        var ran = new Array(2);
        var result = new Array(3);
        for (var a = 0; a < 2; a++) {
            ran[a] = random.make(0, this.DataBase[a].length - 1);
        }
        var C = data[0].length * data[1].length;
        if (C == 0 || data[0].length < 10 || data[1].length < 10) return undefined;
        result[0] = data[0][ran[0]][0];
        result[1] = data[1][ran[1]][0];
        result[2] = `${this.name_convert(result[0])} ${this.name_convert(result[1])}`;

        return result;
    }

    static Make_DataBase() {
        this.DataBase = new Array(2);
        for (var a = 0; a < 2; a++) {
            this.DataBase[a] = this.search(a);
        }
    }

    static search(num) {
        var data = csv.get_csv_data();
        var result = [];
        for (var a = 0; a < data.length; a++) {
            for (var b = 1; b < data[a].length; b++) {
                var flg = this.judgment(data[a][b], num);
                if (flg) result[result.length] = data[a][b];
            }
        }
        return result;
    }

    static judgment(data, num) {
        if (num == 0 && data[1] != "苗字") return false;
        var material = conditions.get("all", num);
        var tool = [
            ["男", "女"],
            ["日本", "西洋"],
            [true, false]
        ];
        for (var a = 0; a < material.length; a++) {
            var b = a;
            if (b > 2) b = 2;
            if (material[a] != 0) {
                var tool_cash = String(tool[b][material[a] - 1]);
                var data_cash = String(data[a + 1]);
                if (tool_cash != data_cash) return false;
            }
        }
        return true;
    }

    static name_convert(str) {
        var num = str.indexOf(" ");
        var result = "";
        str = String(str);
        for (var a = 0; a < num; a++) {
            result += str[a];
        }
        if (num == -1) result = str;
        return result;
    }
}

class display_move {
    static now_display = "main_display";

    static display_id = [
        "main_display",
        "setting",
        "how_to_use"
    ];

    static Get_now_display() {
        return this.now_display;
    }

    static Change_display(str) {
        var display_data = this.display_id;

        if (display_data.indexOf(str) < 0) {
            console.error("存在しないIDが呼び出されました。");
            return undefined;
        }

        for (var a = 0; a < display_data.length; a++) {
            document.getElementById(display_data[a]).scrollTop = 0;
            document.getElementById(display_data[a]).style.display = "none";
        }
        document.getElementById(str).style.display = "block";
    }
}