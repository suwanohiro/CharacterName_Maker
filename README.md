# キャラネームメーカー

&emsp;名前を自動生成してくれるツールです。性別や日本名、西洋名などで条件指定することができます。

## 完成したサイト

&emsp;完成したサイトは[こちら](http://suwanohiro.f5.si/tools/CharacterName_Maker/ver2.0/)から見れます。